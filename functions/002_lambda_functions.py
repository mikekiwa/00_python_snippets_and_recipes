#python
#lambda

These are quickly defined 'anonymous' functions that can be built into one line

A named function....

>>> def f (x): return x**2
>>> print f(8)
64

Re-writing as a Lambda function:

>>> g = lambda x: x**2
>>> print g(8)
64
