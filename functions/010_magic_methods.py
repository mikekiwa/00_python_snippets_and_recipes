#python
#magic

Magic methods

Magic methods have double underscores (dunders) at the start and end of their names.
An example is redefining '+' to add two vectors of a defined vector class

class Vector2D:
  def __init__(self,x,y):
    self.x=x
    self.y=y
  def __add__(self,other):
    return Vector2D(self.x+other.x,self.y+other.y)

first=Vector2D(5,7)
second=Vector2D(3,9)
result=first+second
print (result.x)
print (result.y)

8
16

The __add__method allows for definition of custom behaviour of + for a class. It translates x+y into x.__add__(y)

If x and y are different types then function won't work and __radd__ is automatically called.

Other magic methods are

__sub__ for -
__mul__ for *
__truediv__ for /
__floordiv__ for //
__mod__ for %
__pow__ for **
__and__ for &
__xor__ for ^
__or__ for (stick)

Also (with dunders):
lt for <
le for <=
eq for ==
ne for != (if ne is not implemented it returns the opposite of eq)
gt for >
ge for >=

Though these function names exist to replace standard ones, the actual new function needs to be defined.

There are magic methods that make classes act like containers.

With dunders:
len for len()
getitem for indexing
setitem for assigning to indexed values 
iter for iteration over objects
contains for in

Many other magic methods!
