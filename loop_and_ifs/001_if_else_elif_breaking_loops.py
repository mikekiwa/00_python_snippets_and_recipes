#python
#if

A single = is an assignment, e.g. password = "secret"
Two == is a condition, e.g. password == "secret" is TRUE or FALSE

==     equal to
!=     not equal to
>
<
>=
<=

if statement
============

Indent "block" of commands that run if the condition is met

password = input ("Password? ")
if password == "secret":
    print ("Well done")
    print ("You")
elif password=="Secret":
    print ("Close!")
else:
    print("Noodle brain")
input ("\n\nPress Enter To Exit ")

elif is "else if"

While
=====

# the variable response must have a value before entering the while loop
# this is called a sentry value
response = " "
while response != "Because.":
    response = input ("Why?\n")
print ("Oh. OK.")
input ("\n\nPress Enter To Exit ")

Testing a value
==============

By default if a variable is empty or is zero it is FALSE, and other value is TRUE

e.g. 

# Note: must convert input to number (otherwise a string of "0" is true)
money = int(input("How many pounds do you have? "))
if money:
     print("Be my friend! ")
else:
     print("I must be off ")
input("\nPress Enter To Exit")


or

username=""
while not username:
    username=input("Username: ")

Breaking a loop or skipping remainder of loop
=============================================

count = 0
# Next line creates the infinite loop
while True:
    # increment count
    count += 1
    if count >10:
        break
    if count ==5:
        continue # miss rest of loop
    print (count)

input ("\n\nPress Enter To Exit ")



