#pygame
#python


Setting pixels
--------------

Not commonly done because there are usually faster and easier ways.

Use screen.set_at(pos, col)


Set pixels randomly:
--------------------

import pygame
from pygame.locals import *
from sys import exit
from random import randint

pygame.init()
screen = pygame.display.set_mode((640, 480), 0, 32)

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()

    rand_col = (randint(0, 255), randint(0, 255), randint(0, 255))
    for _ in range(100):
        rand_pos = (randint(0, 639), randint(0, 479))
        screen.set_at(rand_pos, rand_col)

    pygame.display.update()
    
    
Get pixel colour
----------------
my_pixel_colour = screen.get_at((100,100))

Note: best to avoid getting from a hardware surface such as screen. Best to get from softtware
surface.


Screen lock and unlock
----------------------

Pygame automatcially locks and unlocks screen while writing to it.
This prevents a clash with other software updating screen.
When repeated changes are being made it may be more efficient to manually lock and unlock:


import pygame
from pygame.locals import *
from sys import exit
from random import randint

pygame.init()
screen = pygame.display.set_mode((640, 480), 0, 32)

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()

    rand_col = (randint(0, 255), randint(0, 255), randint(0, 255))
    
    # lock screen before 100 changes made
    # -----------------------------------
    
    screen.lock()
    for _ in range(100):
        rand_pos = (randint(0, 639), randint(0, 479))
        screen.set_at(rand_pos, rand_col) 
    
    # unlock screen after changes made
    # --------------------------------
    screen.unlock() 
    pygame.display.update()
