from collections import deque

# deque = double-ended queue
# a deque may be populated from either end

d = deque('abcdefg')
print('Left end', d[0])
print('Right end',d[-1])

#remove item (removes first occurance)
d.remove('c')
print(d)

# Add to right
d.extend('h')
print (d)

#Add to left
# Note that the new items are added to the left iteratively, which reverses
# these items
d.extendleft('0123')
print(d)

#Lists may be used instead of single elements
d.extend(['hello'])
print(d)

#items may be popoed from eithend
print (d.pop())
print (d.popleft())
print (d)

#deques may be rotated:
d = deque(range(10))
print (d)
d.rotate(2)
print ('Right rotation:', d)
d.rotate(-3)
print ('Left rotation :', d)

#Setting a maximum length to a deque
d = deque(maxlen=3)
for i in range(5):
    d.extend([i])
    print (d)
