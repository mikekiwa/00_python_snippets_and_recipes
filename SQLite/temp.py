import sqlite3

# Set up new database
con = sqlite3.connect('population.db') # Creates new database
cur = con.cursor() # tracks position in database

# Create table
# Note data types in sqlite are:
# NULL - type not known
# INTEGER - int
# REAL - float
# TEXT - str
# BLOB - binary data (bytes), such as picture or mp3

# Python works by passing sql commands as a string to sqlite

# create table
cur.execute('CREATE TABLE PopByRegion(Region TEXT, Population INTEGER)')

# Pass data to table
cur.execute('INSERT INTO PopByRegion VALUES("Central Africa", 330993)')
cur.execute('INSERT INTO PopByRegion VALUES("Southestern Africa",743112)')
cur.execute('INSERT INTO PopByRegion VALUES("Japan",100562)')

# Placeholder may also be used (udeful for using loops to add data):
cur.execute('INSERT INTO PopByRegion VALUES(?,?)', ("Newton Abbot",30000))


con.commit() # commit changes
con.close() # close database