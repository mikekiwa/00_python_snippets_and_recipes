#python
#sqlite

import sqlite3
from urllib.request import urlopen # for importing from web

con = sqlite3.connect('sgd.db') # opens or creates a database
cur = con.cursor() # set up cursor to send commands to

# Delete table features if it already exists
cur.execute('drop table if exists features')

# Define table columns
features = 'id text,kind text,status text,orf text,name text,aliases text,parent text,sec_ids text,' \
'chromosome integer,start integer,finish integer,direction text,genetic_pos int,' \
'cordinat_ver text,sequence_vers text,decription text'

# Create SQLite command
execute_txt = 'create table features (' + features + ')'

# Send SQLite command
cur.execute(execute_txt)

# Values are supplied as comma separated values
# It is possible to put comma separated question marks first and send data next

query = 'insert into features values('
query += ','.join(['?'] * 16)
query += ')'

url = 'http://downloads.yeastgenome.org/curation/chromosomal_feature/SGD_features.tab'
f = urlopen(url)
for line in f:
# Strip out end of line and split by tab
 # Decode converts from binary
 fields = line.decode().strip('\n\a').split('\t')
cur.execute(query,fields)

con.commit() # commit changes
con.close() # close database
print('end')
