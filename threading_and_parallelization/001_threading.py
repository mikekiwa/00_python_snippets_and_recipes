import threading
import time


def timer(name, delay, repeat):
    print('Timer: ' + name + 'started')
    while repeat > 0:
        time.sleep(delay)
        print(name + ': ' + str(time.ctime(time.time())))
        repeat -= 1
    print('Timer: ' + name + ' Completed')


def Main():
    t1 = threading.Thread(target=timer, args=("Timer1", 0.5, 5))
    t2 = threading.Thread(target=timer, args=("Timer2", 1, 5))
    t1.start()
    t2.start()

    # Wait for both threads to finish before continuing
    t1.join()
    t2.join()
    print('Main completed')


if __name__ == '__main__':
    Main()
