import threading # Threading module
from queue import Queue # Queue class can store work for threading
import time # to mimic time taken for functions

# Where threads might conflict methods or variables must be locked

# Make locks for threads. When using 'with' these locks, commands can only be
# performed if lock is available
print_lock = threading.Lock()
time_display_lock = threading.Lock()

# Create queue for threads. This is a FIFO queue. Other queue types available
# are LIFO or priority
q = Queue()

# Example function that may be performemd in parallel
def example_job(worker):
    # mimic function that takes time 
    time.sleep(1)

    # Wait for print lock to become available
    # Here we use 'with' lock. Another way is to lock_name.acquire() and then
    # lock_name.release()
    with print_lock:
        # Print current thread
        with time_display_lock:
            elapsed_time = int(time.time()-start)
            print(elapsed_time, threading.current_thread().name, worker)

# Threader which looks for jobs in queue
def threader():
    # Continue threader while main thread not dead
    while True:
        # Get a job from q
        worker = q.get()
        # Call function to be performed in parallel
        example_job(worker)
        # Register fteched task as complete 
        q.task_done() 

# Worker thread Daemons (10 thread daemons created)
for x in range(10):
    # Define threading
    t = threading.Thread(target = threader)

    # Define as Daemon which means thread will die when main thread dies.
    # Without this the main thread cannot terminate unless all threads have
    # terminated 
    t.daemon = True

    # Start thread
    t.start()

start = time.time()

# Create 20 jobs in queue

for worker in range(20):
    # Add item to q (q is a FIFO queue)
    q.put(worker) # put worker in queue

q.join() # wait for thread to terminate

print('Entire job took:', time.time()-start)
