#python
#pandas
#map
#apply

# apply applies function to table
# map applies function or can update values based on dictionary

import pandas as pd

songs_66=pd.Series([3,1,11,15],
                   index=['George','Ringo','John','Paul'],
                   name='Counts')


def format(x):
    if x==1:
        template = '{} song'
    else:
        template = '{} songs'
    return template.format(x)

print(songs_66.map(format))

# .map can also take a dictionary and update any value with matching key to thatdictionary

new_val={3: None,11:21} # this may be formatted as dictionary or series containing dictionary
print(songs_66.map(new_val)) # note values are lost if no equivalent in new dictionary

# .apply is similar to .map but only works with functions, not with value dictionaries
