
Use isin to test membership of a column against the passed in list:

In [30]:
df[df['id'].isin(['foo','bar'])]

Out[30]:
   value   id
0      1  foo
1      2  bar

Here isin generates a boolean mask, we use this to filter the df:

In [31]:    
df['id'].isin(['foo','bar'])

Out[31]:
0     True
1     True
2    False
Name: id, dtype: bool


