#python
#pandas
#filter

***** Note that this routine does not filter a dataframe on its contents. The filter is applied to the labels of the index. *****

DataFrame.filter(items=None, like=None, regex=None, axis=None)

Subset rows or columns of dataframe according to labels in the specified index.
Note that this routine does not filter a dataframe on its contents. 
The filter is applied to the labels of the index.


Parameters:
-----------
items : list-like
List of info axis to restrict to (must not all be present)

like : string
Keep info axis where �arg in col == True�

regex : string (regular expression)
Keep info axis with re.search(regex, col) == True

axis : int or string axis name
The axis to filter on. By default this is the info axis, �index� for Series, �columns� for DataFrame



Examples
========

>>> df
one  two  three
mouse     1    2      3
rabbit    4    5      6

>>> # select columns by name
>>> df.filter(items=['one', 'three'])
one  three
mouse     1      3
rabbit    4      6

>>> # select columns by regular expression
>>> df.filter(regex='e$', axis=1)
one  three
mouse     1      3
rabbit    4      6

>>> # select rows containing 'bbi'
>>> df.filter(like='bbi', axis=0)
one  two  three
rabbit    4    5      6