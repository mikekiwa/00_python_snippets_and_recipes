#python
#pandas

import pandas as pd

songs_66 = pd.Series([3,None,11,9],
                     index=['George','Ringo','John','Paul'],
                     name='Counts') 

songs_69 = pd.Series([18,22,7,5],
                     index=['John','Paul','George','Ringo'],
                     name='Counts')


# using .set_value updates the series and returns a series (will update all with a given index)
print('\nUpdate with get')
songs_66=songs_66.set_value('John',80) # updates value for John
print(songs_66)