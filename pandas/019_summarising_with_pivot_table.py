#python
#pandas
#groupby
#pivot
#statistics

"""groupby and pivot_table can both be used for summarising.
output format can be different between pivot and groupby
groupby always put 'summarise by' columns on left
groupby allows for complex code to be applies to each group created"""



import pandas as pd

scores=pd.DataFrame({
    'name':['Adam','Bob','Dave','Fred'],
    'age':[15,16,16,15],
    'test1':[95,81,89,None],
    'test2':[80,82,84,88],
    'teacher':['Ashby','Ashby','Jones','Jones']})

# PIVOT TABLES

# Median (margin=True adds an 'all' row)
print(scores.pivot_table(index=['teacher','age'],values=['test1','test2'],aggfunc='median',margins=True))
print()

# Melting or stacking data into long format
scores_melt=pd.melt(scores,id_vars=['name','age','teacher'],value_vars=['test1','test2'])
print(scores_melt)
print()

# More descriptive names may be added:
scores_melt=pd.melt(scores,id_vars=['name','age','teacher'],value_vars=['test1','test2'],var_name='test',value_name='score')
print(scores_melt)
print()

# To convert back from long to wide uses a pivot table (or groupby)
# This creates a multi-level heriachial (nested) index
wide_scores=scores_melt.pivot_table(index=['name','age'],columns=['test'],values=['score'])
print(wide_scores)
print()

# To flatten index (creates integer index):
wide_scores=wide_scores.reset_index()
print(wide_scores)
print()

# The values of Test1 and Test2 are still nested. To flatten them:
cols=wide_scores.columns
cols.get_level_values(0) # Index(['name','age','score','score'],dtype='object') # get level 0 values

# Using Dummy Variables to convert categorical values to Boolean
dummy=pd.get_dummies(scores,columns=['age'],prefix='age')
print(dummy)
print()
# see Loc 2245 for function to undo dummies
