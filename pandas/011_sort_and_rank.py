#python
#pandas
#sort
#rank

Sorting and ranking



array2=array1.sort_values() # default kind is quicksort
array2=array1.sort_values(kind='mergesort') # mergesort is slower but does not disrupt the order of items already sorted correctly (c.f. heapsort)
array2=array1.sort_values(ascending=False) # reverse order of sort
array2=array1.sort_index()
array2=array1.sort_index(ascending=False) # sort by index rather than by value
rank=array.rank() # ranks by value; index same order as original

# Note .sort() method is not recommended. By default it does not return a new Series and errors when trying to change series


Documentation:

DataFrame.sort_values(by, axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')


Parameters:
by : str or list of str
Name or list of names which refer to the axis items.
axis : {0 or �index�, 1 or �columns�}, default 0
Axis to direct sorting
ascending : bool or list of bool, default True
Sort ascending vs. descending. Specify list for multiple sort orders. If this is a list of bools, must match the length of the by.
inplace : bool, default False
if True, perform operation in-place
kind : {�quicksort�, �mergesort�, �heapsort�}, default �quicksort�
Choice of sorting algorithm. See also ndarray.np.sort for more information. mergesort is the only stable algorithm. For DataFrames, this option is only applied when sorting on a single column or label.
na_position : {�first�, �last�}, default �last�
first puts NaNs at the beginning, last puts NaNs at the end
Returns:
sorted_obj : DataFrame