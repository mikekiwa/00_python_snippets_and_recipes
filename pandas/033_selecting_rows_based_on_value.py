#python
#pandas

The following selects rows from a pandas dataframe based on a condition. 
It then creates a list of a corresponding column and removes duplicates by converting a list to a set

icu_df=hospital_info.loc[lambda hospital_info: hospital_info.neonatal_ICU ==1, :]
icu=list(icu_df['hospital_postcode'])
icu=set(icu) # removes duplicates by converting to set

The following creates a list of index values for rows matching a condition

good_lsoa_list=list(good_lsoa.loc[lambda good_lsoa: good_lsoa.All ==True, 'All'].index)
