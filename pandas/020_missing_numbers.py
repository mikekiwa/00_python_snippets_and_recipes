#python
#pandas
#missing numbers
#nan

import pandas as pd
import numpy as np
import io # allows strings to act like files

data='''Name|Age|Color
Fed|22|Red
Sally|29|Blue
George|24
Fido||Black'''

df=pd.read_table(io.StringIO(data),sep='|')

# Find missing data
missing=df.isnull()
print(missing)
print()
print(missing.any()) # shows columsn with missing data
print()

# Drop rows with missing data
redacted_df=df.dropna()
print(redacted_df)
print()

# Creating a Boolean mask from one column
valid=df.notnull()
print(df[valid.Age])
print()

# Creating a mask from multiple columns
mask = valid.Age & valid.Color
print(df[mask])
print()

# Or can use .apply to look rowwise and create mask
mask=valid[['Age','Color']].apply(all,axis=1) # all is applied function
print(mask)
print()

# replace missing data with fixed value
print(df.fillna('missing'))
print()

# forward and backward fill takes data from previous or next record
print(df.fillna(method='ffill')) # foward fill from previous record
print()
print(df.fillna(method='bfill')) # backwards fill from previous record
print()
# Data can be filled along rows using axis=1
print(df.fillna(method='ffill',axis=1)) # backwards fill from previous record
print()

# Methods for filling:
    # linear (treats values as evenly spaced; default)
    # time (fill in values based on time index)
    # values/index (use the index to fill in blanks)
    #
    # With Scipy installed the following additional methods are available:
    # nearest (use nearest data point)
    # zero (zero order spline; use last value seen)
    # slinear (first order spline)
    # quardratic (second order spline)
    # cubic (third order spline)
    # polynomial (pass 'order' parameter)
    # spline (pass 'order' parameter)
    # barycentric (use Barycentric Lagrange)
    # krogh (krogh interpolation)
    # piecewise_polynomial
    # pchip (peacewise cubic hermite interpolating polynomial)

# Or use 'replace' to replace nan
print(df.replace(np.nan,value=-1))
print()   
