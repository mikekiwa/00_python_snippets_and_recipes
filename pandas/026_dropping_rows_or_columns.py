#python
#pandas
#drop

## Dropping entries from a series
obj=Series(np.arange(5.),index=['a','b','c','d','e'])
print(obj,"\n")
new_obj=obj.drop('c')
print(new_obj,"\n")


## Dropping columns or rows from data frames
data = DataFrame(np.arange( 16). reshape((4, 4)), index =['Ohio', 'Colorado', 'Utah', 'New York'], columns =['one', 'two', 'three', 'four'])
print(data,'\n')
data2=data.drop(['Colorado','Ohio'])
print(data2,"\n")
data3=data.drop('two',axis=1)
print(data3,"\n")
