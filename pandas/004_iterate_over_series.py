#python
#pandas
#iterate

import pandas as pd

songs_66 = pd.Series([3,None,11,9],
                     index=['George','Ringo','John','Paul'],
                     name='Counts') 

songs_69 = pd.Series([18,22,7,5],
                     index=['John','Paul','George','Ringo'],
                     name='Counts')

# iteration
for item in songs_66:
    print(item)

# iterate over index, value pairs
print('\nIterate over index and value')
for idx,value in songs_66.iteritems():
    print(idx,value)                   

# This could be done in more code without the .iteritems function:
print('\nIterate over index and value without iteritems')
for item in songs_66.iteritems():
    idx=item[0]
    value=item[1]
    print(idx,value)

# To access the index, .keys can also be used
print('\nShow keys')
for idx in songs_66.keys():
    print(idx)
