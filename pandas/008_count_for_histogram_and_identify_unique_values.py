#python
#pandas
#histogram
#count
#unique
#remove duplicates



import pandas as pd

songs_66=pd.Series([3,None,11,9],
                   index=['George','Ringo','John','Paul'],
                   name='Counts')

scores2=pd.Series([67.3,100,96.7,None,100],
                   index=['Ringo','George','Paul','Peter','Billy'],
                   name='test2')

#.count returns non-null items
print('Non-null utems: ',songs_66.count())

#.value_counts are used for histograms
print('\nHistogram')
print(scores2.value_counts())

# Get unique values and count of unique
# Note unique gets nan, but nunique does not count it
print('\nUnique values: ',scores2.unique())
print('\nUnique values count: ',scores2.nunique())

# Drop dulplictaes will remove second and subsequent scores of the same value
print('\nOriginal')
print(scores2)
print('\nDrop duplicates')
print(scores2.drop_duplicates())

# Create Boolean showing if its value was repeated
print('\Value duplicated from before?')
print(scores2.duplicated())

# Dropping duplicated index intries
scores3=pd.Series([67.3,100,96.7,None,100,79],
                   index=['Ringo','Paul','George','Peter','Billy','Paul'],
                   name='test3')

# Using groupby we can take the first or last items for each index
print('\nGet first value of duplicated index')
print(scores3.groupby(scores3.index).first())
print('\nGet last value of duplicated index')
print(scores3.groupby(scores3.index).last())
