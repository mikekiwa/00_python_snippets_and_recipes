"""
DataFrame.dropna
    Remove missing values
DataFrame.isna
    Indicate missing values.
DataFrame.notna
    Indicate existing (non-missing) values.
DataFrame.fillna
    Replace missing values.
Series.dropna
    Drop missing values.
Index.dropna
    Drop missing indices. 
"""


import pandas as pd
import numpy as np

name = ['Bob', 'Jim', 'Anne', 'Rosie', 'Ben', 'Tom']
colour = ['red', 'red', 'red', 'blue', 'red', 'blue']
age = [23, 45, np.NaN, 21, 18, 20]

data =pd.DataFrame()
data['name'] = name
data['colour'] = colour
data['age'] = age

# Show value counts
print (data['colour'].value_counts())

# Remove any na
data = data.dropna()
print(data)