#python
#pandas
#unique
#duplicate

import pandas as pd
data_df=pd.read_csv('example1.csv')
print(data_df)
search=pd.DataFrame.duplicated(data_df) # To identify duplicates with no action
filtered=data_df.drop_duplicates() # to remoe duplicates
print(filtered)


#Note: first duplicate row is not labeled as duplicate
