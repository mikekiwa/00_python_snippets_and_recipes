import bisect
import random

# Use a constant seed to ensure that we see
# the same pseudo-random numbers each time
# we run the loop.
random.seed(1)

# Generate 20 random numbers and
# insert them into a list in sorted
# order.
my_list = []
for i in range(1, 20):
    r = random.randint(1, 100)
    position = bisect.bisect(my_list, r)
    bisect.insort(my_list, r)
    print ('%2d %2d' % (r, position),) 
print(my_list)