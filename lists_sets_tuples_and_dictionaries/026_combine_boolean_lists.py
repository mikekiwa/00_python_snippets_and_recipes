import numpy as np

a = [True, True, True, False]
b = [False, True, True, False]
c = [False, True, False, False]

# Check if all true
mask = list([np.all(line) for line in zip (a,b,c)])
print ('All are true', mask)

# Check if any are true
mask = list([np.any(line) for line in zip (a,b,c)])
print ('Any are true', mask)


