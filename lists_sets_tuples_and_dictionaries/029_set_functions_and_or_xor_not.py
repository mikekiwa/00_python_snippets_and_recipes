#python
#set

Sets like lists but use curly brackets. 
Sets are unordered and un indexed lists. 
They are faster to check contents (in function). 
They cannot contain duplicate items. 
Sets can be used to check membership and remove duplicates.


Add, remove and pop adjusts sets (pop removes arbitrary element).


Sets may be combined:
| in either set
& must be in both sets
- in one set but not the other
^ in one but not both sets
