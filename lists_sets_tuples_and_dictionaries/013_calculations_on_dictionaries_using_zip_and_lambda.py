#python
#lambda
#dictionary

prices={
        'ACME':45.23,
        'AAPL':612.78,
        'IBM': 205.55,
        'HPQ': 37.20,
        'FB': 10.75
        }

# In oder to perform calculation we can invert keys and values with zip()
zipped=zip(prices.values(),prices.keys())

# Output of list(prices.keys()):
# ['ACME', 'AAPL', 'IBM', 'HPQ', 'FB']

# Output of list(prices.values()):   
# [45.23, 612.78, 205.55, 37.2, 10.75]

# Output of zipped
#[(45.23, 'ACME'),
# (612.78, 'AAPL'),
# (205.55, 'IBM'),
# (37.2, 'HPQ'),
# (10.75, 'FB')]

print(min(zipped))
# Output: (10.75, 'FB')

# NOTE: ZIP produces an iterator that can only be consumed once
# To fo another calculation the zip must be re-performed

# Methods such as min. max work on keys rather than values
# An alternative ot zip is to use a diction key lambda
# To return index of minimum value use the following.
# key= transforms the dictionary, so the new key is the value

lowest_price=min(prices, key=lambda k: prices[k])
print(lowest_price) # Output FB
print(prices[lowest_price]) # Output 10.75
# When two keys are the same the values will be compared (e.g. min key then min value)
