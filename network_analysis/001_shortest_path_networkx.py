import networkx as nx
G = nx.Graph()
G.add_edge('A', 'B', weight=4)
G.add_edge('B', 'D', weight=2)
G.add_edge('A', 'C', weight=3)
G.add_edge('C', 'D', weight=4)

# General shortest path between source and target
print('\nGeneral shortest path')
print(nx.shortest_path_length(G, 'A', 'D', weight='weight'))
print(nx.shortest_path(G, 'A', 'D', weight='weight'))

# Dijkstra's algorithm
print('\nDijkstra algorithm')
print(nx.dijkstra_path_length(G, 'A', 'D', weight='weight'))
print(nx.dijkstra_path(G, 'A', 'D', weight='weight'))

# AStar algorithm for shortest path between source and target
print('\nAstar')
print(nx.astar_path_length(G, 'A', 'D', weight='weight'))
print(nx.astar_path(G, 'A', 'D', weight='weight'))

# Bellman Ford algorithm for shortest path between source and target
print('\Bellman Ford')
print(nx.bellman_ford_path_length(G, 'A', 'D', weight='weight'))
print(nx.bellman_ford_path(G, 'A', 'D', weight='weight'))

# Note: functions also exist for all nodes, but these cannot have edge weights