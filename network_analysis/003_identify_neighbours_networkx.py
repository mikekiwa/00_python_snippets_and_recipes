import networkx as nx
G = nx.Graph()
G.add_edge('A', 'B', weight=4)
G.add_edge('B', 'D', weight=2)
G.add_edge('A', 'C', weight=3)
G.add_edge('C', 'D', weight=4)

nodes = ['A','B','C','D']
for node in nodes:
    neighbours = nx.neighbors(G,node)
    print ('Neighbours of node',node)
    for neighbour in neighbours:
        print(neighbour)