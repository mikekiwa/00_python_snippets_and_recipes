import simpy

class School:
    def __init__(self, env):
        self.env = env
        # create an event that will be refenced by two processes
        self.class_ends = env.event()
        self.pupil = env.process(self.pupil())
        # A process can be called multiple times
        # self.pupil = [env.process(self.pupil()) for i in range(3)]
        
        self.bell = env.process(self.bell())
        
    def bell(self):
        for i in range(2):
            yield self.env.timeout(45)
            print ('\nBell rings at '+str(env.now))
            # Complete event and add a return of an optional value
            self.class_ends.succeed(i+1) # event completed
            self.class_ends = self.env.event() # new event started
            yield self.env.timeout(1)            
            print('Bell finishes at '+str(env.now))            
            # Note: processes may use return to return a value
            
    def pupil(self):
        for i in range(2):
            # use an optional setting of a value from yield
            x = yield self.class_ends # class_ends needs to succeed to continue
            print ('Lesson '+ str(x) +' ends \o/ \o/ \o/' +'at ' + str(env.now))
            

env = simpy.Environment()         
school = School (env)
env.run()