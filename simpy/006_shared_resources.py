#python
#simpy

# Here cars will wait for a shared charging resource

import simpy

def car(env,name,bcs,driving_time,charge_duration):
    # Simulate driving to the BCS
    yield env.timeout(driving_time)

    # Request on of its charging points
    print('%s arriving at %d' % (name, env.now))
    with bcs.request() as req: # Using with automatically release request when finished
        # Without the 'with' release() must be used to let go of resources
        yield req

        # Charge the battery
        print('%s starting to charge at %s' % (name, env.now))
        yield env.timeout(charge_duration)
        print('%s leaving the bcs at %s' % (name, env.now))           

env = simpy.Environment()

# Define resoucres
bcs = simpy.Resource(env, capacity=2)

for i in range(4):
    env.process(car(env,'Car %d' %i, bcs,i*2,5))

env.run()

Output:


Car 0 arriving at 0
Car 0 starting to charge at 0
Car 1 arriving at 2
Car 1 starting to charge at 2
Car 2 arriving at 4
Car 0 leaving the bcs at 5
Car 2 starting to charge at 5
Car 3 arriving at 6
Car 1 leaving the bcs at 7
Car 3 starting to charge at 7
Car 2 leaving the bcs at 10
Car 3 leaving the bcs at 12

Changing bcs resource level to 1:

Car 0 arriving at 0
Car 0 starting to charge at 0
Car 1 arriving at 2
Car 2 arriving at 4
Car 0 leaving the bcs at 5
Car 1 starting to charge at 5
Car 3 arriving at 6
Car 1 leaving the bcs at 10
Car 2 starting to charge at 10
Car 2 leaving the bcs at 15
Car 3 starting to charge at 15
Car 3 leaving the bcs at 20
