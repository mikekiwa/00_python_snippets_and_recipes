#python
#simpy

import simpy
import random

class g():
    # Global variables
    bed_count=0
    inter_arrival_time=1
    los=10
    sim_duration=100

def new_admission(env,interarrival_time,los):
    i=0
    while True:
        i+=1
        p_los=random.expovariate(1/los)
        p=patient(env,i,p_los) # defines process to use 
        env.process(p) # initiate process (generator)
        next_p=random.expovariate(1/interarrival_time)
        print('Next patient in %f3.2' %next_p)
        yield env.timeout(next_p)
        
def patient(env,i,p_los):
    g.bed_count+=1
    print('Patient %d arriving %7.2f, bed count %d' %(i,env.now,g.bed_count))
    yield env.timeout(p_los)
    g.bed_count-=1
    print('Patient %d leaving %7.2f, bed count %d' %(i,env.now,g.bed_count))

env=simpy.Environment()
env.process(new_admission(env,g.inter_arrival_time,g.los))
env.run(until=g.sim_duration)
    
    
