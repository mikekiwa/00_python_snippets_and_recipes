
## IMPORT LIBRARIES
## ----------------

import numpy as np
import pandas as pd
import nltk
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords


# If not previously performed:
# nltk.download('stopwords')

## LOAD DATA
## ---------

print ('Loading data')

# If you do not already have the data locally you may download (and save) by:
 file_location = 'https://gitlab.com/michaelallen1966/00_python_snippets' +\
     '_and_recipes/raw/master/machine_learning/data/IMDb.csv'
 imdb = pd.read_csv(file_location)
 # save to current directory
 imdb.to_csv('imdb.csv', index=False)

# If you already have the data locally then you may run the following
# imdb = pd.read_csv('imdb.csv')



## CLEAN DATA
## ----------

stemming = PorterStemmer()
stops = set(stopwords.words("english"))

def apply_cleaning_function_to_list(X):
    cleaned_X = []
    for element in X:
        cleaned_X.append(clean_text(element))
    return cleaned_X

def clean_text(raw_text):
    """This function works on a raw text string, and:
        1) changes to lower case
        2) tokenizes (breaks down into words
        3) removes punctuation and non-word text
        4) finds word stems
        5) removes stop words
        6) rejoins meaningful stem words"""
    
    # Convert to lower case
    text = raw_text.lower()
    
    # Tokenize
    tokens = nltk.word_tokenize(text)
    
    # Keep only words (removes punctuation + numbers)
    # use .isalnum to keep also numbers
    token_words = [w for w in tokens if w.isalpha()]
    
    # Stemming
    stemmed_words = [stemming.stem(w) for w in token_words]
    
    # Remove stop words
    meaningful_words = [w for w in stemmed_words if not w in stops]
    
    # Rejoin meaningful stemmed words
    joined_words = ( " ".join(meaningful_words))
    
    # Return cleaned data
    return joined_words

print ('Cleaning text')
# Get text to clean
text_to_clean = list(imdb['review'])

# Clean text
cleaned_text = apply_cleaning_function_to_list(text_to_clean)


# Tokenize text
print ('Tokenizing text')
tokenized_text = []
for string in cleaned_text:
    tokenized_text.append(nltk.word_tokenize(string))

# Add cleaned data back into DataFrame
imdb['cleaned_review'] = tokenized_text

# Remove temporary cleaned_text list (after transfer to DataFrame)
del cleaned_text

## CONVERT WORDS TO NUMBERS (DICTIONARY INDEX)

import nltk
import numpy as np
import pandas as pd

def text_to_numbers(text, cutoff_for_rare_words = 1):
    """Function to convert text to numbers. Text must be tokenzied so that
    test is presented as a list of words. The index number for a word
    is based on its frequency (words occuring more often have a lower index).
    If a word does not occur as many times as cutoff_for_rare_words,
    then it is given a word index of zero. All rare words will be zero.
    """

    # Flatten list if sublists are present
    if len(text) > 1:
        flat_text = [item for sublist in text for item in sublist]
    else:
        flat_text = text
    
    # get word freuqncy
    fdist = nltk.FreqDist(flat_text)

    # Convert to Pandas dataframe
    df_fdist = pd.DataFrame.from_dict(fdist, orient='index')
    df_fdist.columns = ['Frequency']

    # Sort by word frequency
    df_fdist.sort_values(by=['Frequency'], ascending=False, inplace=True)

    # Add word index
    number_of_words = df_fdist.shape[0]
    df_fdist['word_index'] = list(np.arange(number_of_words)+1)

    # replace rare words with index zero
#    frequency = df_fdist['Frequency'].values
#    word_index = df_fdist['word_index'].values
#    mask = frequency <= cutoff_for_rare_words
#    word_index[mask] = 0
#    df_fdist['word_index'] =  word_index
    
    # Convert pandas to dictionary
    word_dict = df_fdist['word_index'].to_dict()
    
    # Use dictionary to convert words in text to numbers
    text_numbers = []
    for string in text:
        string_numbers = [word_dict[word] for word in string]
        text_numbers.append(string_numbers)
    
    return (text_numbers, df_fdist)


print ('Convert text to numbers')
review_numbered_text, dict_df = text_to_numbers(imdb['cleaned_review'].values)

# Keep only word frequncies 1 to 10000
filtered_text = []
for number_list in review_numbered_text:
    filtered_line = [number for number in number_list if number <=10000]
    filtered_text.append(filtered_line)
imdb['numbered_text'] = filtered_text

# Pickle dataframe and dictionary dataframe
imdb.to_pickle('imdb_numbered.p')
dict_df.to_pickle('imdb_dictionary_dataframe.p')
