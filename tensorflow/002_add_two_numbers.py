import tensorflow as tf

# A tf.placeholder is a node in a TensorFlow graph,
# Values may be passed to Placeholders during a session

a = tf.placeholder(tf.int32)
b = tf.placeholder(tf.int32)

# Note: the following defines an operation, 
# but it won't be performed until the session is acive

c = a + b

# Values are loaded to placeholders using a dictionary
values = {a:5, b:3}

# Start a session (initialises graph)

sess = tf.Session()

print(sess.run([c], values))