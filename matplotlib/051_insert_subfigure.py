#python
#matplotlib
#subplot

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-6,6,1024)
y=np.sinc(x)

x_detail=np.linspace(-3,3,1024)
y_detail=np.sinc(x_detail)

plt.plot(x,y,c='k')

sub_axes=plt.axes([.61,.61,.26,.26]) # x,y co-ordinates of bottom left and x,y size
sub_axes.plot(x_detail,y_detail,c='k')
plt.setp(sub_axes)

plt.show
