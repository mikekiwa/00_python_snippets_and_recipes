#python
#matplotlib
#line plot

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-6,6,1024)
y1=np.sin(x)
y2=np.sin(x)+1

plt.plot(x,y1,marker='o',color='0.75') # points merge into one
plt.plot(x,y2,marker='o',color='k',markevery=32)

plt.show()
