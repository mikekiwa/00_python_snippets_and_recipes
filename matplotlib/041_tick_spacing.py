#python
#matplotlib
#tick

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

x=np.linspace(-15,15,1024)
y=np.sinc(x)

ax=plt.axes() # get axes from plt object
ax.xaxis.set_major_locator(ticker.MultipleLocator(5))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))

plt.grid(True, which='both') # which may be 'major', 'minor' or 'both'

plt.plot(x,y,c='k')

plt.show()
