#python
#matplotlib
#bar chart

# Bar charts do not take colormpas directly. 
# The following shows a way to map values to colormaps in bar charts, adjusting the color to the value (and height) of the bar.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm # colormaps
import matplotlib.colors as col

values=np.linspace(1,99,50)

cmap=cm.ScalarMappable(col.Normalize(0,99),cm.YlOrRd)

plt.bar(np.arange(len(values)),values,color=cmap.to_rgba(values))

plt.show()
