#python
#matplotlib
#bar chart

import numpy as np
import matplotlib.pyplot as plt

values=np.random.random_integers(99,size=50)

color_set=('.00','.25','.50','.75')
color_list=[color_set[(len(color_set)*val)//100] for val in values] # // is floor division

plt.bar(np.arange(len(values)),values,color=color_list)

plt.show()

# =============================
# Same, but with sorted values:
# =============================

import numpy as np
import matplotlib.pyplot as plt

values=np.random.random_integers(99,size=50)
values.sort()

color_set=('.00','.25','.50','.75')
color_list=[color_set[(len(color_set)*val)//100] for val in values] # // is floor division

plt.bar(np.arange(len(values)),values,color=color_list)

plt.show()
