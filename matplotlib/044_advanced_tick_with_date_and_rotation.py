#python
#matplotlib
#tick

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import datetime

start_date=datetime.datetime(1998,1,1)

def make_label(value,pos):
    time=start_date + datetime.timedelta(days = 365*value)
    return (time.strftime('%b %y'))

ax=plt.axes()
ax.xaxis.set_major_formatter(ticker.FuncFormatter(make_label))

x=np.linspace(0,1,256)

plt.plot(x,np.exp(-10*x),c='k')
plt.plot(x,np.exp(-5*x),c='k',ls='--')

labels=ax.get_xticklabels()
plt.setp(labels,rotation=30)

plt.show()
