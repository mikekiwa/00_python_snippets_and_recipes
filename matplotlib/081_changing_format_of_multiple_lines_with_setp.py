#python
#matplotlib
#lineplot
#setp

import matplotlib.pyplot as plt
import numpy as np

x=[10,20,30,40,50]
y=np.random.rand(5,5)
names=['A','B','C','D','E']

lines=plt.plot(x,y)
# Add line names
for i,l in enumerate(lines):
    plt.setp(l,label=names[i])
plt.legend()
plt.show()
