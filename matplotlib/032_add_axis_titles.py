#python
#matplotlib
#axis


import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-4,4,1024)
y=0.25*(x+4)*(x+1)*(x-2)

plt.title('Title')
plt.xlabel('x label')
plt.ylabel('y label')
plt.plot(x,y,c='k')

plt.show()
