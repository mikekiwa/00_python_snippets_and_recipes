#python
#matplotlib
#array

# The example below samples from a Mandlebrot function, but another example could be changing weather data 
# from randomly scattered monitors to a regular 2D grid. The method belows uses interpolation between points, 
# but the default is to simply use nearest neighbour

import numpy as np
from numpy.random import uniform,seed
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import matplotlib.cm as cm

# Mandlebrot function
def iter_count(c,max_iter):
    x=c
    for n in range(max_iter):
        if abs(x)>2:
            return (n)
        x=x**2+c
    return (max_iter)

max_iter=64
xmin,xmax,ymin,ymax=-2.2,.8,-1.5,1.5

# Sample from Mandlebrot function
# A,B are co-ordinates
# C is value

sample_count=2**12
A=uniform(xmin,xmax,sample_count)
B=uniform(ymin,ymax,sample_count)
C=[iter_count(complex(a,b),max_iter) for a,b in zip(A,B)]

# Create regular grid of 512x512
N=512
X=np.linspace(xmin,xmax,N)
Y=np.linspace(ymin,ymax,N)
# Z is the value interpolated from the rnadomly distributed data
# Set to linar below, but usual set is nn or 'neasrest neighbough'
# which is generally very robus
Z=griddata(A,B,C,X,Y,interp='linear')

# Regular data can now use the imshow function
plt.scatter(A,B,color=(0,0,0,.5),s=0.5)
plt.imshow(Z,cmap=cm.binary,
           interpolation='bicubic',
           extent=(xmin,xmax,ymin,ymax))

plt.show()
