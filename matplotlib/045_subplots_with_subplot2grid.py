#python
#matplotlib
#subplot


import numpy as np
import matplotlib.pyplot as plt

t=np.linspace(-np.pi,np.pi,1024)

grid_size=(4,2) # Overall grid of 4 rows and 2 columns

plt.subplot2grid(grid_size,(0,0),rowspan=3,colspan=1) #(0,0) lis location in grid
# rowspan and colspan default to 1 if not specified
plt.plot(np.sin(2*t),np.cos(0.5*t),c='k')
# Calls to pyplot here will refer to the first figure

plt.subplot2grid(grid_size,(0,1),rowspan=3,colspan=1)
plt.plot(np.cos(3*t),np.sin(0.5*t),c='k')

plt.subplot2grid(grid_size,(3,0),rowspan=1,colspan=3)
plt.plot(np.sin(5*t),np.sin(7*t),c='k')

plt.tight_layout() # for best packing of figures

plt.show()
