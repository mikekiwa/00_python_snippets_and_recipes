#python
#matplotlib
#line plot
#title


import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-4,4,1024)
y=0.25*(x+4)*(x+1)*(x-2)

plt.title('A polynomial')
plt.plot(x,y,c='k')

plt.show()
