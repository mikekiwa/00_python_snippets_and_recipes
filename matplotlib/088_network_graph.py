#python
#matplotlib
#network graph

import matplotlib.pyplot as plt
import networkx as nx

G=nx.Graph()
G.add_edge('A','B')
G.add_edge('A','C')
G.add_edge('A','D')
G.add_edge('C','B')

nx.draw_networkx(G)

