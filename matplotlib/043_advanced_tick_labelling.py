#python
#matplotlib
#tick

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# ticker.Locator generateslocation of ticks
# ticker.Formatter generates labels for ticks
# FixedFormatter used below takes labels from a list

def make_label(value,pos):
    # value,pos are the generated values and positions of tick marks
    # This can be more flexible than working with defined strings
    return ('%0.1f' % (100*value))

ax=plt.axes()
# FuncFomratter below is a formatter that uses a function as a paraemter
# The task of generating labels is 'delegated' to a function
ax.xaxis.set_major_formatter(ticker.FuncFormatter(make_label))

x=np.linspace(0,1,256)

plt.plot(x,np.exp(-10*x),c='k')
plt.plot(x,np.exp(-5*x),c='k',ls='--')

plt.show()
