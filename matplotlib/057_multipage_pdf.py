#python
#matplotlib
#pdf

#WARNING: MatPlotLib is not a fully fledged document producing library (see, for example, DocBook)

#The following code produces 3 pages with 5 figures each page.

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from math import floor

# Generate the data
data=np.random.randn(15,1024)

# The pdf document
pdf_pages=PdfPages('barcharts.pdf')

# Generate the pages
plots_count=data.shape[0]
plots_per_page=5
pages_count=floor(plots_count/plots_per_page)
grid_size=(plots_per_page,1)

for i,samples in enumerate(data):
    # create figure instance (i.e. a new page) if needed
    if i % plots_per_page==1:
        fig=plt.figure(figsize=(8.27,11.69),dpi=100)

    # plot one bar chart
    plt.subplot2grid(grid_size,(i % plots_per_page,0))
    plt.hist(samples,32,normed=1,facecolor='.5',alpha=0.75)

    # Close the page if needed
    if (i+1) % plots_per_page==0 or (i+1) == plots_count:
        plt.tight_layout()
        pdf_pages.savefig(fig)

# Write the PDF document to disk
pdf_pages.close()
