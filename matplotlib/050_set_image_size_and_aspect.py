#python
#matplotlib

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-6,6,2000)
y1,y2=np.sinc(x),np.cos(x)

plt.figure(figsize=(20,2.56))

plt.plot(x,y1,c='k',lw=3)
plt.plot(x,y2,c='.75',lw=3)

plt.show()
