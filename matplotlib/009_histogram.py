#python
#matplotlib
#histogram

import matplotlib.pyplot as plt
import numpy as np

x=np.random.randn(10000) # samples from a normal distribution

plt.hist(x,bins=20)

plt.show()
