#python
#matplotlib
#triangulation

# ==========================================================
# Example showing triangles formed from random co-ordinates:
# ==========================================================


import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np

x=np.random.rand(100,2)

triangles=tri.Triangulation(x[:,0],x[:,1])

plt.triplot(triangles)

# ====================================================================
# Example showing triangles formed from a simpler set of co-ordinates:
# ====================================================================

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np

x=np.array([[1,1],
            [1,2],
            [2,2],
            [3,2],
            [4,1]])

triangles=tri.Triangulation(x[:,0],x[:,1])

plt.triplot(triangles)


