#python
#matplotlib
#line plot

import matplotlib.pyplot as plt
X=range(100)
Y=[value ** 2 for value in X] # Applying a function to a list is 'list comprehension'

plt.plot(X,Y)
plt.show()
