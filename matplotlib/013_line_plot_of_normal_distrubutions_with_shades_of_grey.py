#python
#matplotlib
#line plot

import numpy as np
import matplotlib.pyplot as plt

def pdf(x,mu,sigma): # estimated probability distribution for normal distribution
    a=1/(sigma*np.sqrt(2*np.pi))
    b=-1/(2*sigma**2)
    return_val=a*np.exp(b*(x-mu)**2)
    return(return_val)

x=np.linspace(-6,6,1000)

for i in range(5): # generate 5 sets of random samples
    samples=np.random.standard_normal(50)
    mu,sigma=np.mean(samples),np.std(samples)
    plt.plot(x,pdf(x,mu,sigma),color='0.75') # 0.75 is shade of grey, plot estimated probability profile

plt.plot(x,pdf(x,0,1),color='k') # k is black


plt.show()
