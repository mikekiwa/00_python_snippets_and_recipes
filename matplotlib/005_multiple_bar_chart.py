#python
#matplotlib
#bar chart

import matplotlib.pyplot as plt
import numpy as np

data =[[5.,25.,50.,20],
        [4.,23.,51.,17],
        [6., 22.,52.,19]]

X=np.arange(4)

plt.bar(X+0.00,data[0],color='b',width=0.25)
plt.bar(X+0.25,data[1],color='g',width=0.25)
plt.bar(X+0.50,data[2],color='r',width=0.25)

plt.show()


# For larger data sets this may be automated, as below:

import matplotlib.pyplot as plt
import numpy as np

data =[[5.,25.,50.,20],
        [4.,23.,51.,17],
        [6., 22.,52.,19]]

color_list=['b','g','r']
gap=0.8/len(data)

for i, row in enumerate(data): # creates int i and list row
    X=np.arange(len(row))
    plt.bar(X+i*gap,row,width=gap,color=color_list[i])

plt.show()


