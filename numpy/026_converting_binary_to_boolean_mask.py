#python
#numpy

import numpy as np
x=np.random.rand(10,5) # create array of 10 rows and 5 columns and fill with random numbers
y=np.array([1,1,0,0,1]) # binary array mask to be used to select columns
z=np.array(y,dtype=bool) # convert binary to Boolean (true,false)
m=x[:,z] # creates new array based on applying mask to array x, : selects all rows, z is the column mask
print(x)
print(m)
