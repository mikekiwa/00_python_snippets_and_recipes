#python
#numpy
#extract


>>> arr = np.arange(12).reshape((3, 4))
>>> arr

array([[ 0,  1,  2,  3],
       [ 4,  5,  6,  7],
       [ 8,  9, 10, 11]])

>>> condition = np.mod(arr, 3)==0 # no remiander after division by 3

>>> condition

array([[ True, False, False,  True],
       [False, False,  True, False],
       [False,  True, False, False]], dtype=bool)

>>> np.extract(condition, arr)

array([0, 3, 6, 9])

If condition is boolean:

>>> arr[condition]
array([0, 3, 6, 9])