#python
#numpy
#masked array

See also http://docs.scipy.org/doc/numpy/reference/routines.ma.html

Masked arrays
=============
Masked arrays are arrays that may have missing or invalid entries.

For example, suppose we have an array where the fourth entry is invalid:
>>> x = np.array([1, 2, 3, -99, 5])

One way to describe this is to create a masked array:
>>> mx = ma.masked_array(x, mask=[0, 0, 0, 1, 0])
>>> mx
masked_array(data = [1 2 3 -- 5],
             mask = [False False False  True False],
       fill_value = 999999)

Masked mean ignores masked data:
>>> mx.mean()
2.75
>>> np.mean(mx)
2.75

Not all Numpy functions respect masks, for instance np.dot, so check the return types. 
The masked_array returns a view to the original array:
>>> mx[1] = 9
>>> x
array([  1,   9,   3, -99,   5])

The mask
--------
You can modify the mask by assigning:
>>> mx[1] = np.ma.masked
>>> mx
masked_array(data = [1 -- 3 -- 5],
            mask = [False  True False  True False],
    fill_value = 999999)
The mask is cleared on assignment:
>>> mx[1] = 9
>>> mx
masked_array(data = [1 9 3 -- 5],
            mask = [False False False  True False],
    fill_value = 999999)

The mask is also available directly:
>>> mx.mask
array([False, False, False,  True, False], dtype=bool)

The masked entries can be filled with a given value to get an usual array back:
>>> x2 = mx.filled(-1)
>>> x2
array([ 1,  9,  3, -1,  5])

The mask can also be cleared:
>>> mx.mask = np.ma.nomask
>>> mx
masked_array(data = [1 9 3 -99 5],
            mask = [False False False False False],
    fill_value = 999999)

Domain-aware functions
----------------------
The masked array package also contains domain-aware functions:
>>> np.ma.log(np.array([1, 2, -1, -2, 3, -5]))
masked_array(data = [0.0 0.69314718056 -- -- 1.09861228867 --],
            mask = [False False  True  True False  True],
    fill_value = 1e+20)