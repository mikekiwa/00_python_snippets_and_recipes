#python
#numpy
#append

# Note: if axis not specified arrays are flattedned before joining

import numpy as np

#1st array
n=np.array([1,2,3,4])
print(n)

#2nd array
m=np.array([5,6,7,8])
print(m)

#3 join arrays into single row
o=np.append(n,m,axis=0)#this appends the 2 arrays on one row
print(o)

# make each array a row and join to form two rows
q=np.append([n],[m],axis=0)#this appends the 2 arrays in two rows
print(q)

================================================================

Help info:

append(arr, values, axis=None)
    Append values to the end of an array.
    
    Parameters
    ----------
    arr : array_like
        Values are appended to a copy of this array.
    values : array_like
        These values are appended to a copy of `arr`.  It must be of the
        correct shape (the same shape as `arr`, excluding `axis`).  If
        `axis` is not specified, `values` can be any shape and will be
        flattened before use.
    axis : int, optional
        The axis along which `values` are appended.  If `axis` is not
        given, both `arr` and `values` are flattened before use.
    
    Returns
    -------
    append : ndarray
        A copy of `arr` with `values` appended to `axis`.  Note that
        `append` does not occur in-place: a new array is allocated and
        filled.  If `axis` is None, `out` is a flattened array.
    
    See Also
    --------
    insert : Insert elements into an array.
    delete : Delete elements from an array.
    
    Examples
    --------
np.append([1, 2, 3], [[4, 5, 6], [7, 8, 9]])
    array([1, 2, 3, 4, 5, 6, 7, 8, 9])
    
    When `axis` is specified, `values` must have the correct shape.
    
np.append([[1, 2, 3], [4, 5, 6]], [[7, 8, 9]], axis=0)
    array([[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]])
np.append([[1, 2, 3], [4, 5, 6]], [7, 8, 9], axis=0)
    Traceback (most recent call last):
    ...
    ValueError: arrays must have same number of dimensions
