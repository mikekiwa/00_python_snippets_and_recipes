﻿#python
#numpy
#np_c

Will zip two arrays arrays together

>>> np.c_[np.array([1,2,3]), np.array([4,5,6])]
array([[1, 4],
       [2, 5],
       [3, 6]])