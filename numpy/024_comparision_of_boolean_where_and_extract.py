#python
#numpy
#where
#extract

import numpy as np

x=np.array([1,2,3,4,5,6])

print(x%3==0) # returns boolean
print()
print(np.where(x%3==0)) # returns index
print()
print(np.extract(x%3==0,x)) # returns values

Output:

[False False  True False False  True]

(array([2, 5], dtype=int64),)

[3 6]