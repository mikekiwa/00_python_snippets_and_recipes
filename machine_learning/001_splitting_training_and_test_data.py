#python
#machine leaning
#sklearn

import numpy as np
from sklearn import datasets
from sklearn.cross_validation import train_test_split

iris=datasets.load_iris()
X=iris.data[:,[2,3]]
y=iris.target

X_train,X_test,y_train,_y_test=train_test_split(X,y,test_size=0.3,random_state=0)

# Random_state is integer seed. If this is omitted than a different seed will be used each time.
