#python
#image
#numpy
#sklearn


# Load scikit image library with conda install scikit-image

from skimage.io import imread
from skimage.transform import resize
from matplotlib import pyplot as plt
import matplotlib.cm as cm
example_file=('http://upload.wikimedia.org/'+'wikipedia/commons/7/7d/Dog_face.png')

# read image into NumPy array
image=imread(example_file, as_grey=True)
plt.imshow(image,cmap=cm.gray)
plt.show()

#crop image using slicing
image2=image[5:70,0:70]
plt.imshow(image2,cmap=cm.gray)
plt.show()

# rezize image
image3=resize(image2,(30,30),mode='nearest')
plt.imshow(image3,cmap=cm.gray)
plt.show()

# flatten image from array to single dimension
image_row=image3.flatten()
