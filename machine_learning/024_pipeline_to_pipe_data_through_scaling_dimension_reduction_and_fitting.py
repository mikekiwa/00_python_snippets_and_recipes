#python
#machine learning
#sklearn
#matplotlib
#pipeline

# pipeline pipes scaling --> dimension reduction --> model fitting --> estimation





"""Pipeline to join scaling, dimension reduction and classification fitting
"""

import pandas as pd



#%% Original load of data from web
#df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)
#df.head()
#df.to_csv('c:/temp/wisconsin_breast_cancer.csv')
#delete first column and row in CSV

#%% Import data
df=pd.read_csv('data/wisconsin_breast_cancer.csv',header=None)

#%% Assign features to a NumPy array and transform string class M/B

from sklearn.preprocessing import LabelEncoder
X = df.loc[:, 2:].values
y = df.loc[:, 1].values # M/B classification
le = LabelEncoder()
y = le.fit_transform(y) # Convert string to number
le.transform(['M', 'B']) # Reverse coding for transform

#%% Divide data into training and test
from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=1)

#%% Standardise and compress data with PCA
# Pipleline will combine standardisation, dimension reduction and classifier
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
pipe_lr=Pipeline([('scl',StandardScaler()),('pca',PCA(n_components=2)),('clf',LogisticRegression(random_state=1))])
pipe_lr.fit(X_train,y_train)
print('Test accuracy: %.3f' %pipe_lr.score(X_test,y_test))
