from statsmodels.sandbox.stats.runs import cochrans_q
import pandas as pd
import numpy as np


'''Cochran's Q test: 12 subjects are asked to perform 3 tasks. The outcome of each task is "success" or 
"failure". The results are coded 0 for failure and 1 for success. In the example, subject 1 was successful
in task 2, but failed tasks 1 and 3.
Is there a difference between the performance on the three tasks?
'''

tasks = np.array([[0,1,1,0,1,0,0,1,0,0,0,0],
                  [1,1,1,0,0,1,0,1,1,1,1,1],
                  [0,0,1,0,0,1,0,0,0,0,0,0]])

# I prefer a DataFrame here, as it indicates directly what the values mean
df = pd.DataFrame(tasks.T, columns = ['Task1', 'Task2', 'Task3'])

# --- >>> START stats <<< ---
(Q, pVal) = cochrans_q(df)
# --- >>> STOP stats <<< ---
print('\nCOCHRAN\'S Q -----------------------------------------------------')
print('Q = {0:5.3f}, p = {1:5.3f}'.format(Q, pVal))
if pVal < 0.05:
    print("There is a significant difference between the three tasks.")
