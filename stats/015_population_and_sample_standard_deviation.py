﻿#python
#stats
#numpy
#standard deviation
#stdev

~Note: By default NumPy returns population standard deviation rather than sample (Pandas returns sample standard deviation by default)

import numpy as np

data=np.arange(7,14)

# Population stdev
print('Population stdev: ',np.std(data,ddof=0))

# Sample mean
print('Sample stdev: ',np.std(data,ddof=1))