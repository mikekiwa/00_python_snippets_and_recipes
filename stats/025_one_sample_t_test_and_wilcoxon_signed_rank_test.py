import numpy as np
import scipy.stats as stats

# generate the data
np.random.seed(12345)
normDist = stats.norm(loc=7, scale=3)
data = normDist.rvs(100)
checkVal = 6.5

# T-test
# --- >>> START stats <<< ---
t, tProb = stats.ttest_1samp(data, checkVal)
# --- >>> STOP stats <<< ---
print (t,tProb)


# Wilcocon Signed Rank
# Note the test value is subtracted from all data (test os then effectively against zero)
rank, pVal = stats.wilcoxon(data-checkVal)
print (rank, pVal)
