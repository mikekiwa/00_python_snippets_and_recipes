anova
-----
Test for whether there a significant difference within multiple groups
Two-way (or more) anova allows for e.g. age groups and sex groups to be treated
as related groups 



t-test
------
One sample for test vs value
Two sample for comparison
Assumes normally distributed data

Mann Whitney U Test
-------------------
Test of difference between two groups
Continuous data, non-normally distributed

Wilcoxon signed rank test
-------------------------
Test of non-normal continuous data vs zero
(subtract test value from all samples if need to test vs a test value)

Fisher's exact test
-------------------
Check for significant differences between multiple groups (nominal data)
Can also be used on categorical data

Kruskal-Wallis test
-------------------
Check for significant differences between multiple groups (ordinal data)

Chi square
----------
Check for differences in frequency table

McNemar's test
--------------
Matched pair for frequency data for 2x2 table, e.g. test whether two doctors 
give same diagnosis on the same patient given the same data. 

Cochran's Q test
----------------
Extension of McNemar's test for testing differences between three or more
groups of categorigcal data.
