#python
#numpy
#scipy
#hamming
#pdist

import numpy as np
from scipy.spatial.distance import pdist

# a=np.array([[1,1,1,1,1],[0,0,0,0,0],[1,0,1,0,1],[0,1,0,1,0]]) # example array 1
a=np.random.randint(0,2,size=(1000,173)) # example array 2
hamming_distances = pdist(a,'hamming')
av_diff=np.average(hamming_distances )
print(av_diff)
