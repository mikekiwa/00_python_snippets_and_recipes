items=[' hello\n','my name is',' Tom ']

# Print raw list
print ([[x,len(x)] for x in items])

# Print striped of white space at end and beginning
print ([[x.strip(),len(x.strip())] for x in items])

