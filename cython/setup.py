from Cython.Build import cythonize # converts python to c
from distutils.core import setup # converts c to an executable

setup(ext_modules = cythonize('fib.pyx'))

# to compile use 'python setup.py build_ext --inplace'


