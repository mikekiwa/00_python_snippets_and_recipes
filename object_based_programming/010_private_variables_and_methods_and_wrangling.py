#python
#oop
#private

Private variables are not normally accessible from outside of the object, though there is a way around it (which should not normally be used - variables are made private to indicate that they should be kept so)


class Pet (object):
    """A virtual pet"""
    total = 0 # A variable defined outside of a method is a class attribute
   
    @staticmethod
    def status():
        print("\nThe total number of pets is %s" % Pet.total)
   

    def __init__(self,name,legs): # this runs when a new instance is created
        print("A new pet has been born!")
        self.name=name
        self.__number_of_legs=legs # this is a privare variable # two underscores indicate a private variable accessible only from within object
         Pet.total +=1
       
    def __str__(self): # this is what is retruned if a the object is printed
        rep="Critter object\n"
        rep+="Name: "+self.name+"\n"
        return rep

    def count_legs(self):
        print(self.name, "has", self.__number_of_legs, "legs.")

  

doug = Pet("Douggy",4)
timmy= Pet ("Timy",3)
print (doug)
doug.count_legs()
timmy.count_legs()
Pet.status()
print (doug.total) # also accesses class attribute

=================================================================================================

More.....

There is no way to completely hide object variables. But using __ before name causes them to appear hidden until _ClassName is added before __varname to reveal them:

But usually just using a single underscore, while not doing anything in Python, is suffiicent to tell others that this is a variable that should not be accessed externally.

class SecretString:
    def __init__(self,plain_string,secret_string):
        self.plain_string=plain_string
        self.__secret_string=secret_string #use of __ will cause name wrangling
        
    def print_strings(self):
        print(self.plain_string)
        print(self.__secret_string)
        
x=SecretString('Plain','Wrangled')

print('\nPrint from object:\n')
x.print_strings()

print('\nTry to access from outside object:\n')
print(x.plain_string)
try:
    print(x.__secret_string)
except:
    print('Cannot access __secret_string')   
    
print('\nAccess using x._SecretString__secret_string:')
# use of _Classname before wrangled var will reveal hidden string
print(x._SecretString__secret_string)    


print(doug._Pet__number_of_legs) # This accesses a private variable

Private methods may be defined in the same way with two underscores before the method:

def __private_method(self):
